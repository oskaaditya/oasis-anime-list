import React, { FC } from "react"
import Image from "next/image"
import Link from "next/link"
import { Anime } from "@/interfaces/anime"

interface SideAnimeListProps {
  data?: Anime[]
}

const SideAnimeList: FC<SideAnimeListProps> = ({ data }) => {
  return (
    <>
      {data?.map((anime) => (
        <div key={anime.mal_id} className="flex items-start gap-2">
          <div>
            <Image
              src={anime.images.webp.image_url}
              width={65}
              height={65}
              alt="img-anime-recom"
              className="h-full max-h-40 rounded-md"
            />
          </div>
          <div>
            <Link
              href={`/${anime.mal_id}`}
              className="text-sm font-semibold text-white hover:text-secondary-yellow hover:underline"
            >
              {anime.title}{" "}
              {anime.year && <span className="font-bold">({anime.year})</span>}
            </Link>
            <div className="mt-2">
              <p className="text-xs text-white">Episodes: {anime.episodes}</p>
              <p className="text-xs text-white">
                Score: {anime.score} ({anime.scored_by})
              </p>
              <p className="text-xs text-white">Type: {anime.type}</p>
            </div>
          </div>
        </div>
      ))}
    </>
  )
}

export default SideAnimeList
