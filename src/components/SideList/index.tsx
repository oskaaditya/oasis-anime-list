import React, { FC, ReactNode } from "react"
import Link from "next/link"

interface SideListProps {
  children: ReactNode
  title: string
  href: string
}

const SideList: FC<SideListProps> = ({ children, title, href }) => {
  return (
    <div className="my-1">
      <div className="flex items-center justify-between border-b pb-2">
        <h2 className="my-2 font-semibold text-white">{title}</h2>
        <Link
          href={href}
          className="hover:text-secondary-yellow text-sm text-white"
        >
          Show more &gt;
        </Link>
      </div>
      <div className="flex flex-col gap-4 pt-3">{children}</div>
    </div>
  )
}

export default SideList
