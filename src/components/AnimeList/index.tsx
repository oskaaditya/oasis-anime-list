import React, { FC } from "react"
import Image from "next/image"
import Link from "next/link"
import { Anime } from "@/interfaces/anime"

interface AnimeListProps {
  data?: Anime[]
}

const AnimeList: FC<AnimeListProps> = ({ data }) => {
  return (
    <div className="grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4">
      {data?.map((anime) => (
        <>
          <Link
            key={anime.mal_id}
            href={`/${anime.mal_id}`}
            className="card-anime relative cursor-pointer"
          >
            <Image
              src={anime.images.webp.large_image_url}
              alt="img"
              width={350}
              height={350}
              className="h-full max-h-96 w-full rounded-lg object-cover"
            />
            <div className="absolute bottom-0 left-0 h-52 w-full rounded-b-md bg-gradient-to-t from-black to-transparent">
              <h3 className="absolute bottom-0 left-0 p-3 text-sm font-semibold text-white md:text-base">
                {anime.title}
              </h3>
            </div>
          </Link>
        </>
      ))}
    </div>
  )
}

export default AnimeList
