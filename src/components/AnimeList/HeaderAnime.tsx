import React, { FC } from "react"
import Link from "next/link"

interface HeaderAnimeProps {
  title: string
  linkTitle: string
  linkHref: string
}

const HeaderAnime: FC<HeaderAnimeProps> = ({ title, linkTitle, linkHref }) => {
  return (
    <div className="flex items-center justify-between">
      <h2 className="my-2 text-2xl font-semibold text-white">{title}</h2>
      <Link
        href={linkHref}
        className="text-sm text-white hover:text-secondary-yellow"
      >
        {linkTitle}
      </Link>
    </div>
  )
}

export default HeaderAnime
