import React, { FC } from "react"
import Image from "next/image"
import { FiEye } from "react-icons/fi"

import { Badge } from "../ui/badge"
import { Button } from "../ui/button"

interface HeroProps {
  image?: string
  title?: string
  year?: number
  type?: string
  description?: string
  genre?: any[]
  score?: number
}

const Hero: FC<HeroProps> = ({
  image,
  title,
  year,
  type,
  description,
  genre,
  score,
}) => {
  const bgImage = {
    backgroundImage: `url(${image})`,
    backgroundSize: "cover",
    backgroundPosition: "right center",
  }
  return (
    <div className="flex-items-centered mx-8 flex rounded-md" style={bgImage}>
      <div className="w-[100%] rounded-md bg-gradient-to-r from-black to-transparent p-10">
        {genre?.map((genre) => {
          return (
            <Badge key={genre.mal_id} className="my-2 mr-2" variant={"genre"}>
              {genre.name}
            </Badge>
          )
        })}
        <h1 className="text-3xl font-semibold text-white">{title}</h1>
        <div>
          <p className="text-white">
            {year} | {type}
          </p>
          <Badge variant={"score"}>
            <p className="font-bold">Score: {score}/10</p>
          </Badge>
          <p className="my-4 line-clamp-5 w-96 text-sm text-white">
            {description}
          </p>
        </div>
        <div className="flex items-center">
          <Button variant={"primary"} className="font-semibold">
            See More{" "}
            <span className="ml-2">
              <FiEye />
            </span>
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Hero
