import React, { FC } from "react"

interface HeaderMenuProps {
  title: string
}

const HeaderMenu: FC<HeaderMenuProps> = ({ title }) => {
  return (
    <div className="mb-4 text-center text-4xl font-bold uppercase text-white">
      {title}
    </div>
  )
}

export default HeaderMenu
