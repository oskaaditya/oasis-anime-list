import React, { FC, ReactElement } from "react"
import Link from "next/link"
import type { IconType } from "react-icons"

import { Button } from "@/components/ui/button"

interface SocialLinkProps {
  url: string
  icon: IconType
}

const SocialLink: FC<SocialLinkProps> = ({ url, icon }) => {
  return (
    <Button
      asChild
      variant={"link"}
      className="p-0 text-xl text-white transition-all hover:text-secondary-yellow"
    >
      <Link href={url}>{React.createElement(icon) as ReactElement}</Link>
    </Button>
  )
}

export default SocialLink
