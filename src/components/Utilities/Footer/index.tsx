import React from "react"
import Image from "next/image"
import Link from "next/link"
import { FiInstagram } from "react-icons/fi"

import { Button } from "../../ui/button"
import { SocialLinks } from "./constants"
import SocialLink from "./SocialLink"

const Footer = () => {
  return (
    <div className="bg-[#23252B]">
      <div className="container mx-auto py-14">
        <div className="flex flex-col items-center justify-center gap-4">
          <Image
            className="logo"
            src="https://res.cloudinary.com/dysce4sh2/image/upload/v1697119055/logooasisanime_crjpzy.png"
            alt="logo"
            width={129}
            height={24}
          />
          <p className="text-md  w-1/3 text-center text-white">
            Discover the ultimate hub for anime and manga enthusiasts, where
            information and excitement collide!
          </p>
          <div className="flex items-center gap-4">
            {SocialLinks.map((item, index) => (
              <SocialLink key={index} url={item.url} icon={item.icon} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
