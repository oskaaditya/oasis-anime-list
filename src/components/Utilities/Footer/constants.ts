import { SocialLinksItems } from "@/interfaces/anime"
import { FaGithubAlt, FaLinkedin } from "react-icons/fa"
import { FiDribbble, FiInstagram } from "react-icons/fi"

export const SocialLinks: Array<SocialLinksItems> = [
  {
    icon: FiInstagram,
    url: "https://www.instagram.com/oasis_ui",
  },
  {
    icon: FaLinkedin,
    url: "https://www.linkedin.com/in/oskaaditya/",
  },
  {
    icon: FaGithubAlt,
    url: "https://github.com/oskaaditya",
  },
  {
    icon: FiDribbble,
    url: "https://dribbble.com/oskaaditya",
  },
]
