"use client"

import React, { useState } from "react"
import { useRouter } from "next/navigation"
import { FiSearch } from "react-icons/fi"

import { Input } from "../../ui/input"

const SearchBar = () => {
  const [keyword, setKeyword] = useState("")
  const router = useRouter()

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      console.log("Keydown :", keyword)
      router.push(`/search/${keyword}`)
    }
  }

  return (
    <div className="relative">
      <Input
        type="text"
        placeholder="Search..."
        className="w-full bg-transparent text-white placeholder:text-gray-200"
        onChange={(e) => setKeyword(e.currentTarget.value)}
        onKeyDown={handleKeyDown}
      />
      <div className="text-md absolute end-2 top-3 text-white">
        <FiSearch />
      </div>
    </div>
  )
}

export default SearchBar
