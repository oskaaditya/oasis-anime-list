import React from "react"
import Image from "next/image"
import Link from "next/link"

import SearchBar from "./SearchBar"

const Navbar = () => {
  return (
    <div className="bg-[#23252B]">
      <div className="container mx-auto">
        <div className="flex flex-col items-center justify-between gap-3 px-8 py-4 md:flex-row md:gap-0">
          <Link href={"/"}>
            <Image
              className="logo"
              src="https://res.cloudinary.com/dysce4sh2/image/upload/v1697119055/logooasisanime_crjpzy.png"
              alt="logo"
              width={129}
              height={24}
            />
          </Link>
          <SearchBar />
        </div>
      </div>
    </div>
  )
}

export default Navbar
