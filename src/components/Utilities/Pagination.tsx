"use client"

import React, { FC } from "react"

import { Button } from "../ui/button"

interface PaginationProps {
  page: number
  totalPages?: number
  setPage: (page: number) => void
}

const Pagination: FC<PaginationProps> = ({ page, totalPages, setPage }) => {
  const scrollTop = () => {
    scrollTo({
      behavior: "smooth",
      top: 0,
    })
  }

  const handlePrevPage = () => {
    setPage(page - 1)
    scrollTop()
  }

  const handleNextPage = () => {
    setPage(page + 1)
    scrollTop()
  }

  return (
    <div className="my-4 flex items-center justify-center gap-4">
      <Button
        className="transition-all hover:text-secondary-yellow"
        onClick={handlePrevPage}
        disabled={page === 1}
      >
        Prev
      </Button>
      <p className="text-white">
        {page} of {totalPages}
      </p>
      <Button
        className="transition-all hover:text-secondary-yellow"
        onClick={handleNextPage}
        disabled={page === totalPages}
      >
        Next
      </Button>
    </div>
  )
}

export default Pagination
