import type { AxiosRequestConfig } from "axios"
import axiosClient from "axios"

const instance = axiosClient.create({
  baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
  headers: {
    "content-type": "application/json",
    accept: "application/json",
  },
})

const axios = <T>(config: AxiosRequestConfig) =>
  instance.request<unknown, T>(config)

export default axios
