import React from "react"

const Loading = () => {
  return (
    <div className="flex min-h-screen flex-col items-center justify-center">
      <div className="loader"></div>
      <p className="mt-4 text-lg font-semibold text-white">Loading..</p>
    </div>
  )
}

export default Loading
