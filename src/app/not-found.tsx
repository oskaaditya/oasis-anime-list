"use client"

import React from "react"
import Image from "next/image"
import { useRouter } from "next/navigation"
import { FiArrowLeft } from "react-icons/fi"

import { Button } from "@/components/ui/button"

const NotFound = () => {
  const router = useRouter()

  const handleBackPage = () => {
    router.back()
  }

  return (
    <div className="flex min-h-screen flex-col items-center justify-center">
      <p className="text-8xl font-bold text-white">404</p>
      <p className="mb-8 text-4xl font-semibold text-white">Page Not Found</p>
      <Button
        variant={"outline"}
        className="mb-5 gap-2"
        onClick={handleBackPage}
      >
        <FiArrowLeft /> Back to previous page
      </Button>
      <Image
        className="logo"
        src="https://res.cloudinary.com/dysce4sh2/image/upload/v1697119055/logooasisanime_crjpzy.png"
        alt="logo"
        width={129}
        height={24}
      />
    </div>
  )
}

export default NotFound
