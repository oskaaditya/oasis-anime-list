"use client"

import React, { FC } from "react"
import { useRouter } from "next/navigation"
import { useSearchAnimeQuery } from "@/api/anime"

import AnimeList from "@/components/AnimeList"

interface SearchProps {
  params: {
    keyword: string
  }
}

const SearchPage: FC<SearchProps> = ({ params }) => {
  const { data } = useSearchAnimeQuery({
    q: params.keyword,
  })
  const decodeKeyword = decodeURI(params.keyword)

  return (
    <section className="container mx-auto my-8 px-8">
      <h1 className="mb-4 text-center text-2xl font-semibold text-white">
        Pencarian untuk &quot;{decodeKeyword}&quot;
      </h1>
      <AnimeList data={data?.data.data} />
    </section>
  )
}

export default SearchPage
