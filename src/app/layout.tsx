import Navbar from "@/components/Utilities/Navbar"

import "./globals.css"

import { Suspense } from "react"
import type { Metadata } from "next"
import { DM_Sans } from "next/font/google"
import ReactQueryProviders from "@/providers/ReactQueryProviders"

import Footer from "@/components/Utilities/Footer"

import Loading from "./loading"

const DMSans = DM_Sans({ subsets: ["latin"] })

export const metadata: Metadata = {
  title: "Oasis Anime List",
  description: "Website Anime List di Indonesia",
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={DMSans.className} suppressHydrationWarning={true}>
        <ReactQueryProviders>
          <Navbar />
          <Suspense fallback={<Loading />}>{children}</Suspense>
          <Footer />
        </ReactQueryProviders>
      </body>
    </html>
  )
}
