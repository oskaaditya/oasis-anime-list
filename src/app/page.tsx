"use client"

import {
  usePopularAnimeQuery,
  useTopAiringAnimeQuery,
  useTopAnimeQuery,
} from "@/api/anime"

import AnimeList from "@/components/AnimeList"
import HeaderAnime from "@/components/AnimeList/HeaderAnime"
import Hero from "@/components/Hero"
import SideAnimeList from "@/components/SideAnimeList"
import SideList from "@/components/SideList"

import Loading from "./loading"

const Home = () => {
  const { data: popularAnime, isLoading: loadingPopularAnime } =
    usePopularAnimeQuery({
      limit: 8,
      filter: "bypopularity",
      sfw: true,
    })

  const { data: topAnime, isLoading: loadingTopAnime } = useTopAnimeQuery({
    limit: 3,
    sfw: true,
  })

  const { data: topAiringAnime, isLoading: loadingTopAiringAnime } =
    useTopAiringAnimeQuery({
      limit: 3,
      filter: "airing",
      sfw: true,
    })

  return (
    <div className="my-8 w-full">
      {loadingPopularAnime || loadingTopAiringAnime || loadingTopAnime ? (
        <Loading />
      ) : (
        <div className="container mx-auto">
          <section>
            <Hero
              title={topAiringAnime?.data.data[1].title}
              image={topAiringAnime?.data.data[1].images.jpg.large_image_url}
              year={topAiringAnime?.data.data[1].year}
              type={topAiringAnime?.data.data[1].type}
              description={topAiringAnime?.data.data[1].synopsis}
              genre={topAiringAnime?.data.data[1].genres}
              score={topAiringAnime?.data.data[1].score}
            />
          </section>
          <section>
            <div className="mt-8 grid grid-cols-3 gap-8 px-8">
              <div className="col-span-2">
                <HeaderAnime
                  title="Popular Anime"
                  linkHref="/anime/popular"
                  linkTitle="Show more &gt;"
                />
                <AnimeList data={popularAnime?.data.data} />
              </div>
              <div className="flex flex-col gap-4">
                <SideList
                  title="Anime Recomendation"
                  href="/anime/recomendation"
                >
                  <SideAnimeList data={topAnime?.data.data} />
                </SideList>
                <SideList title="Top Airing Anime" href="/anime/top-airing">
                  <SideAnimeList data={topAiringAnime?.data.data} />
                </SideList>
              </div>
            </div>
          </section>
        </div>
      )}
    </div>
  )
}

export default Home
