"use client"

import React, { useEffect, useState } from "react"
import { useTopAiringAnimeQuery } from "@/api/anime"

import AnimeList from "@/components/AnimeList"
import HeaderMenu from "@/components/Utilities/HeaderMenu"
import Pagination from "@/components/Utilities/Pagination"
import Loading from "@/app/loading"

const AiringAnime = () => {
  const [page, setPage] = useState(1)
  const { data, refetch, isRefetching, isLoading } = useTopAiringAnimeQuery({
    filter: "airing",
    sfw: true,
    page,
  })

  useEffect(() => {
    refetch()
  }, [page])

  return (
    <section className="container mx-auto my-8 px-8">
      <HeaderMenu title="Top Airing Anime" />
      {isRefetching || isLoading ? (
        <Loading />
      ) : (
        <AnimeList data={data?.data.data} />
      )}
      <Pagination
        page={page}
        totalPages={data?.data.pagination.last_visible_page}
        setPage={setPage}
      />
    </section>
  )
}

export default AiringAnime
