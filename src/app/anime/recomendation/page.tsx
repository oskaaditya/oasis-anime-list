"use client"

import React, { useEffect, useState } from "react"
import { useTopAnimeQuery } from "@/api/anime"

import AnimeList from "@/components/AnimeList"
import HeaderMenu from "@/components/Utilities/HeaderMenu"
import Pagination from "@/components/Utilities/Pagination"
import Loading from "@/app/loading"

const RecomendationAnime = () => {
  const [page, setPage] = useState(1)
  const { data, refetch, isRefetching, isLoading } = useTopAnimeQuery({
    filter: "favorite",
    sfw: true,
    page,
  })

  useEffect(() => {
    refetch()
  }, [page])

  return (
    <section className="container mx-auto my-8 px-8">
      <HeaderMenu title="Anime Recomendation" />
      {isRefetching || isLoading ? (
        <Loading />
      ) : (
        <AnimeList data={data?.data.data} />
      )}
      <Pagination
        page={page}
        totalPages={data?.data.pagination.last_visible_page}
        setPage={setPage}
      />
    </section>
  )
}

export default RecomendationAnime
