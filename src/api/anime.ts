"use client"

import { Anime, AnimeParams } from "@/interfaces/anime"
import { QueryPropsList } from "@/interfaces/common"
import { useQuery } from "@tanstack/react-query"

import axios from "@/lib/utils/callApi"

export const usePopularAnimeQuery = (params?: AnimeParams, enabled?: boolean) =>
  useQuery<QueryPropsList<Anime>>(
    ["popular-anime"],
    () =>
      axios<QueryPropsList<Anime>>({
        method: "get",
        url: "/top/anime",
        params,
      }),
    { enabled }
  )

export const useTopAnimeQuery = (params?: AnimeParams, enabled?: boolean) =>
  useQuery<QueryPropsList<Anime>>(
    ["recomendation-anime"],
    () =>
      axios<QueryPropsList<Anime>>({
        method: "get",
        url: "/top/anime",
        params,
      }),
    { enabled }
  )
export const useTopAiringAnimeQuery = (
  params?: AnimeParams,
  enabled?: boolean
) =>
  useQuery<QueryPropsList<Anime>>(
    ["upcoming-anime"],
    () =>
      axios<QueryPropsList<Anime>>({
        method: "get",
        url: "/top/anime",
        params,
      }),
    { enabled }
  )
export const useSearchAnimeQuery = (params?: AnimeParams, enabled?: boolean) =>
  useQuery<QueryPropsList<Anime>>(
    ["search-anime"],
    () =>
      axios<QueryPropsList<Anime>>({
        method: "get",
        url: "anime",
        params,
      }),
    { enabled }
  )
