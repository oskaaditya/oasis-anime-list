import { type } from "os"
import { IconType } from "react-icons"

export interface Anime {
  mal_id: number
  url: string
  title: string
  year: number
  images: ImageAnime
  episodes: number
  score: number
  scored_by: number
  type: string
  background: string
  synopsis: string
  genres: Genres[]
}

export interface SocialLinksItems {
  icon: IconType
  url: string
}

export interface AnimeParams {
  limit?: number
  page?: number
  filter?: string
  sfw?: boolean
  q?: string
}

interface ImageAnime {
  jpg: {
    image_url: string
    small_image_url: string
    large_image_url: string
  }
  webp: {
    image_url: string
    small_image_url: string
    large_image_url: string
  }
}

interface Genres {
  mal_id: number
  name: string
  type: string
  url: string
}
