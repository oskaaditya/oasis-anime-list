export interface Response<T> {
  data: T
}

export type PaginatedDataResponse<T> = T & {
  pagination: Pagination
}

export interface PaginatedData<T> {
  data: PaginatedDataResponse<T>
}

export interface Pagination {
  page: number
  size: number
  total_data: number
  total_pages: number
}

export interface PageProps {
  browserTimingHeader: string
}

export interface MutationProps<
  TData = unknown,
  TError = unknown,
  TVariables = void,
  TContext = unknown,
> {
  onSuccess?: (
    data: TData,
    variables: TVariables,
    context: TContext | undefined
  ) => Promise<unknown> | unknown
  onError?: (
    error: TError,
    variables: TVariables,
    context: TContext | undefined
  ) => Promise<unknown> | unknown
}

export interface MetaTable {
  current_page: number
  from: number
  last_page: number
  per_page: number
  total: number
}

export interface iPagination {
  count: number
  total: number
  per_page: number
}

export interface QueryPropsList<T> {
  message: string
  data: {
    data: T[]
    pagination: {
      last_visible_page: number
      has_next_page: boolean
      current_page: number
      items: iPagination
    }
  }
}

export interface QueryPropsSimpleList<T> {
  message: string
  data: T[]
}

export interface QueryPropsSimpleWithMetaList<T> {
  message: string
  data: T[]
  meta: MetaTable
  to: number
  total: number
}

export interface QueryPropsDetail<T> {
  message: string
  data: T
}

export interface FormError {
  error: boolean
  message: string
}

export interface ISort {
  name: string
  direction: "asc" | "desc"
}

export interface ISelect {
  value: string | number
  label: string
}
